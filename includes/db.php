<?php

$host = 'localhost';
$dbname = 'e_market';
$username = 'marketuser';
$password = 'marketpass';

try {
    $pdo = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die("Database connection failed: " . $e->getMessage());
}


function getProducts() {
    global $pdo;
    $stmt = $pdo->query("SELECT * FROM Products");
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function getProductById($productid) {
    global $pdo;
    $stmt = $pdo->query("SELECT * FROM Products WHERE product_id = $productid");
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

// Function to calculate the total amount of the order
function calculateTotalAmount($cart_items) {
    $total_amount = 0;
    foreach ($cart_items as $item) {
        $total_amount += $item['price'] * $item['quantity'];
    }
    return $total_amount;
}


?>
