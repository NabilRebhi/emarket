
function showConfirmation(message) {
    alert(message);
}

function validateSignInForm() {
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;

    if (username.trim() === '') {
        alert('Please enter your username.');
        return false;
    }

    if (password.trim() === '') {
        alert('Please enter your password.');
        return false;
    }

    return true;
}

function validateSignUpForm() {
    var username = document.getElementById('username').value;
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;


    if (username.trim() === '' || email.trim() === '' || password.trim() === '') {
        alert('Please fill out all fields.');
        return false;
    }

    var emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailPattern.test(email)) {
        alert('Please enter a valid email address.');
        return false;
    }


    if (password.length < 8) {
        alert('Password must be at least 8 characters long.');
        return false;
    }


    return true;
}

function placeOrder() {
   
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "place_order.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
       
            document.getElementById("order-status").innerHTML = xhr.responseText;
        }
    };
    xhr.send();
}

function goBack() {
    window.history.back();
}
