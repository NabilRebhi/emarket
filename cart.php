<?php
session_start();
include_once 'includes/db.php'; 


if (!isset($_SESSION['user_id'])) {
   
    header("Location: signin.php");
    exit();
}


$user_id = $_SESSION['user_id'];


if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['delete_product'])) {
    $product_id = $_POST['product_id'];


    $sql = "DELETE FROM Cart WHERE user_id = ? AND product_id = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$user_id, $product_id]);

    
    header("Location: cart.php");
    exit();
}


if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['checkout'])) {
    
    header("Location: checkout.php");
    exit();
}

// Retrieve cart items for the logged-in user
$sql = "SELECT products.product_id, products.name AS product_name, products.price, cart.quantity
        FROM Cart
        INNER JOIN Products ON Cart.product_id = Products.product_id
        WHERE Cart.user_id = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$user_id]);
$cart_items = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Your Cart - Online Clothing Store</title>
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
    <header>
        <h1>Your Cart</h1>
    </header>

    <nav>
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="contact.php">Contact</a></li>
            <li><a href="#">Cart</a></li>
            <?php
            if (!isset($_SESSION['user_id'])) {
               
                echo '<li><a href="signin.php">Sign In</a></li>';
                echo '<li><a href="signup.php">Sign Up</a></li>';
            } else {
               
                echo '<li><a href="logout.php">Logout</a></li>';
            }
            ?>
        </ul>
    </nav>

    <main>
        <section id="cart">
            <?php if (empty($cart_items)): ?>
                <p>Your cart is empty.</p>
            <?php else: ?>
                <form action="" method="POST">
                    <table>
                        <thead>
                            <tr>
                                <th>Product Name</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($cart_items as $item): ?>
                                <tr>
                                    <td><?php echo $item['product_name']; ?></td>
                                    <td><?php echo $item['price']; ?></td>
                                    <td><?php echo $item['quantity']; ?></td>
                                    <td><?php echo $item['price'] * $item['quantity']; ?></td>
                                    <td>
                                        <form action="" method="POST">
                                            <input type="hidden" name="product_id" value="<?php echo $item['product_id']; ?>">
                                            <input type="submit" name="delete_product" value="Delete">
                                        </form>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <input type="submit" name="checkout" value="Checkout">
                </form>
            <?php endif; ?>
        </section>
    </main>

    <script src="js/script.js"></script>
</body>
</html>
