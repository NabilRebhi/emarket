<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up - Online Clothing Store</title>
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
    <header>
        <h1>Welcome to our Online Clothing Store</h1>
    </header>

    <nav>
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="contact.php">Contact</a></li>
            <li><a href="cart.php">Cart</a></li>
            <?php
            if (!isset($_SESSION['user_id'])) {
              
                echo '<li><a href="signin.php">Sign In</a></li>';
                echo '<li><a href="#">Sign Up</a></li>';
            } else {
              
                echo '<li><a href="logout.php">Logout</a></li>';
            }
            ?>
        </ul>
    </nav>

    <main>
        <section id="signup-form">
            <h2>Sign Up</h2>
            <form action="signup_process.php" method="POST" onsubmit="return validateSignUpForm()">
                <label for="username">Username:</label>
                <input type="text" id="username" name="username" required>
                <label for="email">Email:</label>
                <input type="email" id="email" name="email" required>
                <label for="password">Password:</label>
                <input type="password" id="password" name="password" required>
                <button type="submit">Sign Up</button>
            </form>
        </section>
    </main>


    <script src="js/script.js"></script>
</body>
</html>
