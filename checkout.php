<?php
session_start();
include_once 'includes/db.php';


if (!isset($_SESSION['user_id'])) {
    
    header("Location: signin.php");
    exit();
}


$user_id = $_SESSION['user_id'];


$sql = "SELECT products.name AS product_name, products.price, cart.quantity
        FROM Cart
        INNER JOIN Products ON Cart.product_id = Products.product_id
        WHERE Cart.user_id = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$user_id]);
$cart_items = $stmt->fetchAll(PDO::FETCH_ASSOC);


$total_amount = calculateTotalAmount($cart_items);


if ($_SERVER["REQUEST_METHOD"] == "POST") {
  
    $sql = "INSERT INTO Orders (user_id, total_amount) VALUES (?, ?)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$user_id, $total_amount]);

 
    $sql = "DELETE FROM Cart WHERE user_id = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$user_id]);

    header("Location: cart.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Checkout</title>
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
    <header>
        <h1>Checkout</h1>
    </header>

    <main>
        <section id="cart">
            <h2>Order Summary</h2>
            <?php if (empty($cart_items)): ?>
                <p>Your cart is empty.</p>
            <?php else: ?>
                <table>
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($cart_items as $item): ?>
                            <tr>
                                <td><?php echo $item['product_name']; ?></td>
                                <td><?php echo $item['price']; ?></td>
                                <td><?php echo $item['quantity']; ?></td>
                                <td><?php echo $item['price'] * $item['quantity']; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <p>Total Amount: <?php echo $total_amount; ?></p>
                <h2>Shipping Details</h2>
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                   
                    <input type="submit" value="Place Order">
                </form>
            <?php endif; ?>
        </section>
    </main>

    <script src="js/script.js"></script>
</body>
</html>
