<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign In - Online Clothing Store</title>
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
    <header>
        <h1>Welcome to our Online Clothing Store</h1>
    </header>

    <nav>
        <ul>
            <li><a href="index.php">Home</a></li>
            
            <li><a href="contact.php">Contact</a></li>
            <li><a href="cart.php">Cart</a></li>
            <?php
            if (!isset($_SESSION['user_id'])) {
               
                echo '<li><a href="#">Sign In</a></li>';
                echo '<li><a href="signup.php">Sign Up</a></li>';
            } else {
               
                echo '<li><a href="logout.php">Logout</a></li>';
            }
            ?>
        </ul>
    </nav>

    <main>
    <section id="signin-form">
        <h2>Sign In</h2>
        <?php
       
        if (isset($_SESSION['error']) && !empty($_SESSION['error'])) {
            echo "<p class='error-message'>{$_SESSION['error']}</p>";
            unset($_SESSION['error']); 
        }
        ?>
        <form action="signin_process.php" method="POST" onsubmit="return validateSignInForm()">
            <label for="username">Username:</label>
            <input type="text" id="username" name="username" required>
            <label for="password">Password:</label>
            <input type="password" id="password" name="password" required>
            <button type="submit">Sign In</button>
        </form>
    </section>
</main>



    <script src="js/script.js"></script>
</body>
</html>
