<?php
session_start();


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
    $product_id = $_POST['product_id'];

    
    include_once 'includes/db.php';
    $product = getProductById($product_id); 

    
    if ($product) {
        
        $user_id = $_SESSION['user_id'];
        $quantity = 1;

      
        $sql = "INSERT INTO cart (user_id, product_id, quantity) VALUES (?, ?, ?)";
        
     
        $stmt = $pdo->prepare($sql);


        $stmt->execute([$user_id, $product_id, $quantity]);

        
        header("Location: index.php"); 
        exit();
    } else {
       
        echo "Product not found or invalid.";
    }
} else {
    
    echo "Form was not submitted.";
}
?>
