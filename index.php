<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Clothing Store</title>
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
    <header>
        <h1>Welcome to our Online Clothing Store</h1>
    </header>

    <nav>
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="contact.php">Contact</a></li>
            <li><a href="cart.php">Cart</a></li>
            <?php
            if (!isset($_SESSION['user_id'])) {
               
                echo '<li><a href="signin.php">Sign In</a></li>';
                echo '<li><a href="signup.php">Sign Up</a></li>';
            } else {
               
                echo '<li><a href="logout.php">Logout</a></li>';
            }
            ?>
        </ul>
    </nav>

    <main>
        <section id="products">
            
            <?php
            include_once 'includes/db.php';
            $products = getProducts();
            foreach ($products as $product) {
                echo "<div class='product-card'>";
                echo "<img src='{$product['image_url']}' alt='{$product['name']}' class='product-image'>";
                echo "<div class='product-details'>";
                echo "<h2>{$product['name']}</h2>";
                echo "<p>{$product['description']}</p>";
                echo "<p>Price: {$product['price']}</p>";
                echo "<form action='add_to_cart.php' method='POST'>";
                echo "<input type='hidden' name='product_id' value='{$product['product_id']}'>"; 
                echo "<input type='submit' value='Add to Cart'>";
                echo "</form>";
                echo "</div>"; 
                echo "</div>"; 
            }
            ?>
        </section>
    </main>

    <script src="js/script.js"></script>
</body>
</html>
