-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 14, 2024 at 12:32 AM
-- Server version: 8.2.0
-- PHP Version: 8.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e_market`
--
CREATE DATABASE IF NOT EXISTS `e_market` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `e_market`;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
CREATE TABLE IF NOT EXISTS `cart` (
  `cart_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  PRIMARY KEY (`cart_id`),
  KEY `user_id` (`user_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contactmessages`
--

DROP TABLE IF EXISTS `contactmessages`;
CREATE TABLE IF NOT EXISTS `contactmessages` (
  `message_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `sent_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`message_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `order_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`order_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text,
  `price` decimal(10,2) NOT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `name`, `description`, `price`, `image_url`) VALUES
(1, 'T-shirt', 'Comfortable cotton T-shirt', 19.99, 'tshirt.jpg'),
(2, 'Jeans', 'Classic blue jeans', 39.99, 'jeans.jpg'),
(3, 'Sneakers', 'Stylish sneakers for casual wear', 49.99, 'sneakers.jpg'),
(4, 'Dress Shirt', 'Formal dress shirt', 29.99, 'dress_shirt.jpg'),
(5, 'Skirt', 'Floral-printed skirt', 24.99, 'skirt.jpg'),
(6, 'Hoodie', 'Warm and cozy hoodie', 34.99, 'hoodie.jpg'),
(7, 'Leggings', 'Stretchy leggings for workouts', 19.99, 'leggings.jpg'),
(8, 'Jacket', 'Waterproof jacket for outdoor activities', 59.99, 'jacket.jpg'),
(9, 'Dress', 'Elegant evening dress', 79.99, 'dress.jpg'),
(10, 'Shorts', 'Casual shorts for summer', 14.99, 'shorts.jpg'),
(11, 'Blouse', 'Chic blouse for everyday wear', 29.99, 'blouse.jpg'),
(12, 'Sweater', 'Soft knit sweater', 39.99, 'sweater.jpg'),
(13, 'Coat', 'Winter coat for cold days', 89.99, 'coat.jpg'),
(14, 'Pants', 'Casual pants for all occasions', 29.99, 'pants.jpg'),
(15, 'Sandals', 'Comfortable sandals for the beach', 19.99, 'sandals.jpg'),
(16, 'Scarf', 'Stylish scarf to accessorize your outfit', 14.99, 'scarf.jpg'),
(17, 'Cardigan', 'Lightweight cardigan for layering', 24.99, 'cardigan.jpg'),
(18, 'Boots', 'Fashionable boots for any season', 69.99, 'boots.jpg'),
(19, 'Sweatpants', 'Relaxed fit sweatpants', 24.99, 'sweatpants.jpg'),
(20, 'Top', 'Casual top for everyday wear', 19.99, 'top.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
