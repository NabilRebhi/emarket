<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact Us - Online Clothing Store</title>
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
    <header>
        <h1>Contact Us</h1>
    </header>

    <nav>
        <ul>
            <li><a href="index.php">Home</a></li>
            
            <li><a href="#">Contact</a></li>
            <li><a href="cart.php">Cart</a></li>
            <?php
            if (!isset($_SESSION['user_id'])) {
                
                echo '<li><a href="#">Sign In</a></li>';
                echo '<li><a href="signup.php">Sign Up</a></li>';
            } else {
               
                echo '<li><a href="logout.php">Logout</a></li>';
            }
            ?>
        </ul>
    </nav>

    <main>
        <section id="contact-form">
            <h2>Send Us a Message</h2>
            <form action="contact_process.php" method="POST">
                <label for="name">Your Name:</label>
                <input type="text" id="name" name="name" required>
                <label for="email">Your Email:</label>
                <input type="email" id="email" name="email" required>
                <label for="subject">Subject:</label>
                <input type="text" id="subject" name="subject" required>
                <label for="message">Message:</label>
                <textarea id="message" name="message" rows="4" required></textarea>
                <button type="submit">Send Message</button>
            </form>
        </section>
    </main>

 
</body>
</html>
